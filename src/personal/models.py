from django.db import models

# Create your models here.

class Persona(models.Model):
    id = models.AutoField(default=0,primary_key=True)
    rut = models.CharField(max_length=12,blank=True,null=True)
    nombre = models.CharField(max_length=50,blank=True,null=True)
    apellidos = models.CharField(max_length=50,blank=True,null=True)
    telefono = models.IntegerField(default=1)
    direccion = models.CharField(max_length=60,blank=True,null=True)
    fecha_nacimiento = models.DateField(blank=True,null=True,default=False)

    def __str__(self):
        return self.nombre

class Turno(models.Model):
    id = models.AutoField(default=0,primary_key=True)
    nombre = models.CharField(max_length=12,blank=True,null=True)
    horario_inicio = models.CharField(max_length=15,blank=True,null=True)
    horario_final = models.CharField(max_length=15,blank=True,null=True)
    #fk
    persona_id = models.IntegerField(default=1)

    def __str__(self):
        return self.nombre