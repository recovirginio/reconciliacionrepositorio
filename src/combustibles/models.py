from django.db import models

# Create your models here.
class Dispensador(models.Model):
    id = models.AutoField(default=0,primary_key=True)
    nombre_disp = models.CharField(max_length=12,blank=True,null=True)
    fecha_ult_mantencion = models.DateField(blank=True,null=True,default=False)

    def __str__(self):
        return self.nombre

class Combustible(models.Model):
    id = models.AutoField(default=0,primary_key=True)
    octanaje = models.CharField(max_length=10,blank=True,null=True)
    stock = models.IntegerField(default=1)
    stock_minimo = models.IntegerField(default=1)
    #fk
    id_precio_venta = models.IntegerField(default=1)

    def __str__(self):
        return self.octanaje

class Detalle_venta(models.Model):
    id_detalle_venta = models.AutoField(default=0,primary_key=True)
    folio_inicial = models.IntegerField(default=1)
    folio_final = models.IntegerField(default=1)
    tot_venta_litro = models.DecimalField(max_digits=5,decimal_places=3,default=0.0)
    #fk
    id_dispensador = models.IntegerField(default=1)

    def __str__(self):
        return self.octanaje