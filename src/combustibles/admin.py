from django.contrib import admin

# Register your models here.
from combustibles.models import *

admin.site.register(Dispensador)
admin.site.register(Combustible)
admin.site.register(Detalle_venta)